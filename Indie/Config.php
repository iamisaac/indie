<?php

/**
 * @package Indie
 */
namespace Indie;

/**
 * @uses BadFunctionCallException
 */
use BadFunctionCallException;

/**
 * Config
 */
class Config
{
    /**
     * @var array
     * @access public
     */
    public static $config = array();

    /**
     * Set config
     *
     * @param array $config
     */
    public static function set(array $config)
    {
        static::$config = $config;
    }

    /**
     * Get config
     *
     * @param string $name
     *
     * @return mixed
     */
    public static function get($name)
    {
        if (!array_key_exists($name, static::$config)) {
            throw new BadFunctionCallException('');
        }

        return static::$config[$name];
    }

    /**
     * Add value to the config array
     *
     * @param string $name
     * @param mixed $config
     */
    public function add($name, $config)
    {
        static::$config[$name] = $config;
    }
}