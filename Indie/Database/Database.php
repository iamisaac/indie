<?php

/**
 * @package Indie\Database
 */
namespace Indie\Database;

/**
 * @uses PDO
 * @uses UnexpectedValueException
 */
use PDO;
use UnexpectedValueException;

/**
 * Database
 *
 * This class serves as a very basic layer for database interaction. It uses PDO as the 
 * base, and gives the developer a simple way to establish a database connection. Even 
 * though it is a layer, it also gives direct access to the PDO and PDOStatement objects.
 * This way it does not add any restriction on which functionality that is available.
 */
class Database
{
    /**
     * @var PDO|null Raw instance of PDO
     */
    public $pdo = null;

    /**
     * @var PDOStatement|null Raw instance of PDOStatement
     */
    public $statement = null;

    /**
     * @var array Configuration settings used to open the active connection
     */
    public $config = array();

    /**
     * Database constructor
     *
     * @param array $config
     * @param object|null $driver
     */
    public function __construct(array $config = array(), $driver = null)
    {
        if (!is_null($driver)) {
            $this->setDriver($config, $driver);
        }
    }

    /**
     * Set driver
     * 
     * Attempt to establish a connection based on the given configurations
     *
     * @param array $config
     * @param object $driver
     *
     * @throws UnexpectedValueException
     */
    public function setDriver($config, $driver)
    {
        if (!is_object($driver)) {
            $type = gettype($driver);
            throw new UnexpectedValueException('Expected $driver to be an object, ' . $type . ' was given.');
        }

        $this->pdo = $driver->connect($config);
        $this->config = $config;
    }

    /**
     * Check if we are connected to a database
     *
     * @return boolean
     */
    public function isConnected()
    {
        return ($this->pdo instanceof PDO);
    }

    /**
     * Execute a query
     *
     * @param string $query
     * @param array $bind
     *
     * @return PDOStatement
     */
    public function query($query, array $bind = array())
    {
        $this->statement = $this->pdo->prepare($query);
        $this->statement->execute($bind);
        return $this->statement;
    }
}