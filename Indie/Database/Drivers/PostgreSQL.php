<?php

/**
 * @package Indie\Database\Drivers
 */
namespace Indie\Database\Drivers;

/**
 * @uses PDO
 * @uses BadFunctionCallException
 */
use PDO;
use BadFunctionCallException;

/**
 * PostgreSQL
 */
class PostgreSQL
{
    /**
     * @var array Required configuration values
     */
    private $requiredConfig = array('hostname', 'username', 'password', 'database');

    /** 
     * @var string Error message to use with exceptions
     */
    public $errorMessage = '';

    /**
     * Connect to database
     *
     * @param array $config
     *
     * @throws BadFunctionCallException
     * @return PDO
     */
    public function connect(array $config)
    {
        if (!$this->checkConfig($config)) {
            throw new BadFunctionCallException($this->errorMessage);
        }

        $dsn = "pgsql:host={$config['hostname']};dbname={$config['database']};";
        $dsn .= (array_key_exists('port', $config)) ? ";port={$config['port']}" : '';

        $pdo = new PDO($dsn, $config['username'], $config['password']);

        if (array_key_exists('charset', $config)) {
            $pdo->query("SET NAMES '{$config['charset']}");
        }

        return $pdo;
    }

    /**
     * Check configuration array
     *
     * Make sure that all required fields in the configuration has been provided
     *
     * @param array $config
     *
     * @return boolean
     */
    private function checkConfig(array $config)
    {
        $errors = array();
        $errors = array_diff($this->requiredConfig, array_flip($config));

        if (!empty($errors)) {
            if (sizeof($errors) == 1) {
                $missingConfig = join('', $errors);
                $this->errorMessage = "Missing required configuration settings. No {$missingConfig} has been configured.";
            } else {
                $lastMissingConfig = array_pop($errors);
                $missingConfigs = join(', ', $errors);
                $errorString = 'Missing required configuration settings. No %s or %s has been configured.';
                $this->errorMessage = sprintf($errorString, $missingConfigs, $lastMissingConfig);
            }
        }

        return (empty($errors));
    }
}