<?php

/**
 * @package Indie\Files
 */
namespace Indie\Files;

/**
 * Common file handle functions
 *
 * This class contains commonly used functions when working with files of any type
 */
class Common
{
    /**
     * @var string Error message from last error that occured
     */
    public $errorMessage = '';

    /**
     * Rename file
     *
     * @param string $oldFilePath Full path to the file that will be renamed
     * @param string $newFilename 
     * @param boolean $overwrite
     *
     * @return boolean
     */
    public function rename($oldFilePath, $newFilename, $overwrite = false)
    {
        if (!file_exists($oldFilePath)) {
            $this->errorMessage = "File '{$oldFilePath}' does not exist.";
            return false;
        }

        $path = dirname($oldFilePath);
        $oldFilename = basename($oldFilePath);
        $newFilePath = "{$path}/{$newFilename}";
        
        if ((!$overwrite) && (file_exists($newFilePath))) {
            $this->errorMessage = "Unable to rename '{$oldFilename}' to '{$newFilename}'. A file with that name already exists in '{$path}'.";
            return false;
        }

        return rename($oldFilePath, $newFilePath);
    }

    /**
     * Move file to another folder
     *
     * @param string $oldFilePath
     * @param string $destination
     * @param boolean $overwrite
     *
     * @return boolean
     */
    public function move($oldFilePath, $destination, $overwrite = false)
    {
        if (!file_exists($oldFilePath)) {
            $this->errorMessage = "File '{$oldFilePath}' does not exist.";
            return false;
        }

        if (!is_dir($destination)) {
            $this->errorMessage = "Destination folder '{$destination}' does not exist.";
            return false;
        }

        $filename = basename($oldFilePath);
        $destination = rtrim($destination, '/');
        $newFilePath = "{$destination}/{$filename}";

        if ((!$overwrite) && (file_exists($newFilePath))) {
            $this->errorMessage = "Unable to move '{$filename}' to '{$destination}'. A file with that name already exists in that directory.";
            return false;
        }

        return rename($oldFilePath, $newFilePath);
    }

    /**
     * Copy file
     *
     * @param string $oldFilePath
     * @param string $destination
     * @param boolean $overwrite
     *
     * @return boolean
     */
    public function copy($oldFilePath, $destination, $overwrite = false)
    {
        if (!file_exists($oldFilePath)) {
            $this->errorMessage = "File '{$oldFilePath}' does not exist.";
            return false;
        }

        $filename = basename($oldFilePath);
        $destination = rtrim($destination, '/');
        $newFilePath = "{$destination}/{$filename}";

        if ((!$overwrite) && (file_exists($newFilePath))) {
            $this->errorMessage = "Unable to copy '{$filename}' to '{$destination}'. A file with that name already exists in that directory.";
            return false;
        }

        return copy($oldFilePath, $newFilePath);
    }

    /**
     * Delete file
     *
     * @param string $filePath
     *
     * @return boolean
     */
    public function delete($filePath)
    {
        if (!file_exists($filePath)) {
            $this->errorMessage = "File '{$filePath}' does not exist.";
            return false;
        }

        return unlink($filePath);
    }
}