<?php

/**
 * @package Indie\Files
 */
namespace Indie\Files;

/**
 * @uses Indie\Files\Common
 * @uses Indie\Files\Upload
 */
use Indie\Files\Common;
use Indie\Files\Upload;

/**
 * File
 *
 * This class handles a file like its own entity. It also works as a factory class against
 * other classes in the Indie\Files component.
 */
class File
{
    /**
     * @var string Active filename
     */
    private $filename = '';

    /**
     * @var string Active file path
     */
    private $filePath = '';

    /**
     * @var string Active file full path
     */
    private $fullPath = '';

    /**
     * @var Indie\Files\Common|null
     */
    public $common = null;

    /**
     * @var Indie\Files\Uploader|null
     */
    public $upload = null;

    /**
     * @var string Last error message
     */
    public $errorMessage = '';

    /**
     * File constructor
     *
     * @param string $file If left blank the class works only as a factory class
     */
    public function __construct($file = '')
    {
        $this->common = new Common;
        $this->upload = new Uploader;

        if (!empty($file)) {
            $this->setFile($file);
        }
    }

    /**
     * Set a file to work with
     *
     * @param string $file
     *
     * @return boolean
     */
    public function setFile($file)
    {
        if (!file_exists($file)) {
            $this->errorMessage = "The file '{$file}' does not exist.";
            return false;
        } else {
            $this->fullPath = $file;
            $this->filename = basename($file);
            $this->filePath = dirname($file);
            return true;
        }
    }

    /**
     * Get extension of the currently active file
     *
     * @param string $filname If no filename is given we use the filename for the file object
     *
     * @return string|false Returns extension if found, otherwise false
     */
    public function getExtension($filename = null)
    {
        $filename = (is_null($filename)) ? $this->filename : $filename;

        if (empty($filename)) {
            $this->errorMessage = 'Unable to extract file extension';
            return false;
        } else {
            return pathinfo($filename, PATHINFO_EXTENSION);
        }
    }

    /**
     * Rename file
     * Alias for Indie\Files\Common::rename()
     *
     * @param string $newFilename
     *
     * @return boolean
     */
    public function rename($newFilename)
    {
        if (!$this->common->rename($this->fullPath, $newFilename)) {
            $this->errorMessage = $this->common->errorMessage;
            return false;
        }

        $this->filename = $newFilename;
        $this->fullPath = "{$this->filePath}/{$newFilename}";
        return true;
    }

    /**
     * Move file to a new folder
     * Alias for Indie\Files\Common::move()
     *
     * @param string $destination
     *
     * @return boolean
     */
    public function move($destination)
    {
        if (!$this->common->move($this->fullPath, $destination)) {
            $this->errorMessage = $this->common->errorMessage;
            return false;
        }

        $this->filePath = $destination;
        $this->fullPath = "{$this->filePath}/{$this->filename}";
        return true;
    }

    /**
     * Copy file to another folder
     * Alias for Indie\Files\Common::copy()
     *
     * @param string $destination
     *
     * @return boolean
     */
    public function copy($destination)
    {
        if (!$this->common->copy($this->fullPath, $destination)) {
            $this->errorMessage = $this->common->errorMessage;
            return false;
        }

        $this->filePath = $destination;
        $this->fullPath = "{$this->filePath}/{$this->filename}";
        return true;
    }

    /**
     * Delete file
     * Alias for Indie\Files\Common::delete()
     *
     * @return boolean
     */
    public function delete()
    {
        if (!$this->common->delete($this->fullPath)) {
            $this->errorMessage = $this->common->errorMessage;
            return false;
        }

        $this->filename = '';
        $this->filePath = '';
        $this->fullPath = '';
        return true;
    }

    /**
     * Upload file
     * Alias for Indie\Files\Uploader::upload()
     *
     * @param array $file
     * @param string $destination
     *
     * @return boolean
     */
    public function upload(array $file, $destination)
    {
        $this->upload->setDestination($destination);
        if (!$this->upload->upload($file)) {
            var_dump($this->upload->errors);
            return false;
        } else {
            $this->filename = $file['name'];
            $this->filePath = $destination;
            $this->fullPath = "{$this->filePath}/{$this->filename}";
            return true;
        }
    }
}