<?php

/**
 * @package Indie\Files
 */
namespace Indie\Files;

/**
 * File uploader
 *
 * Simple but powerful file uploader class
 */
class Uploader
{
    /**
     * @var string Upload destination directory
     */
    protected $destination = '';

    /**
     * @var integer Max allowed file size
     */
    protected $maxFileSize = 2000000;

    /**
     * @var array Allowed file types/extensions
     */
    protected $allowedFileTypes = array();

    /**
     * @var array Array of error messages
     */
    public $errors = array();

    /**
     * Upload constructor
     *
     * @param string $destination
     * @param integer $maxFileSize
     * @param array $fileTypes
     */
    public function __construct($destination = '', $maxFileSize = 2000000, $fileTypes = array())
    {
        if (!empty($destination)) {
            $this->setDestination($destination);
        }

        $this->setMaxFileSize($maxFileSize);
        $this->setAllowedFileTypes($fileTypes);
    }

    /**
     * Set upload destination folder
     *
     * @param string $destination Destination folder
     * @param boolean $mkdir If true the method will try to create the folder
     * @param integer $chmod Set the mode of the specified file to that given in mode.
     * @param boolean $nested Allows the creation of nested directories specified in the pathname.
     *
     * @return boolean
     */
    public function setDestination($destination, $mkdir = false, $chmod = 0777, $nested = false)
    {
        if (!is_dir($destination)) {
            if (($mkdir) && (!mkdir($destination, $chmod, $nested))) {
                return false;
            } else {
                return false;
            }
        }

        $this->destination = $destination;
    }

    /**
     * Set max allowed file size
     *
     * @param integer $maxFileSize
     */
    public function setMaxFileSize($maxFileSize)
    {
        $this->maxFileSize = (integer) $maxFileSize;
    }

    /**
     * Set array of allowed file types/extensions
     *
     * Array and comma separated string can be used
     *
     * @param array|string $extension
     */
    public function setAllowedFileTypes($extensions)
    {
        if (is_string($extensions)) {
            $this->allowedFileTypes = explode(',', $extensions);
        } else {
            $this->allowedFileTypes = (array) $extensions;
        }
    }

    /**
     * Validate file details
     *
     * @param array $file
     * @param boolean $overwrite
     *
     * @return boolean
     */
    public function validate(array $file, $overwrite)
    {
        $this->fileInfo = array(
            'name' => $file['name'],
            'ext' => pathinfo($file['name'], PATHINFO_EXTENSION),
            'size' => $file['size'],
            'tmp_name' => $file['tmp_name'],
        );

        if ($file['error'] > 0) {
            $this->errors['error'] = $file['error'];
        }

        if (empty($this->destination)) {
            $this->errors['no_dest'] = 'No destination folder has been set';
        }

        if ((!empty($this->allowedFileTypes)) && (!in_array($this->fileInfo['ext'], $this->allowedFileTypes))) {
            $this->errros['type'] = 'Invalid file type';
        }

        if ($this->fileInfo['size'] > $this->maxFileSize) {
            $this->errros['size'] = 'File is to big';
        }

        if ((!$overwrite) && (file_exists($this->destination.'/'.$this->fileInfo['name']))) {
            $this->errros['file_exists'] = 'File already exists';
        }

        return (empty($this->errors));
    }

    /**
     * Start the process of uploading the file
     *
     * @param array $file $_FILES for one file only
     * @param boolean $overwrite
     *
     * @return boolean
     */
    public function upload(array $file, $overwrite = false)
    {
        if ($this->validate($file, $overwrite)) {
            $destination = "{$this->destination}/{$this->fileInfo['name']}";
            return (move_uploaded_file($this->fileInfo['tmp_name'], $destination));
        }
    }
}