<?php

/**
 * @package Indie
 */
namespace Indie;

/**
 * @uses UnexpectedValueException
 */
use UnexpectedValueException;

/**
 * SPL Autoloader class
 *
 * Basic SPL autoloader class based on PSR-0 autoloading standard
 */
class Loader
{
    /**
     * @var string Root path for vendor libraries
     */
    private $vendorPath = '';

    /**
     * Loader constructor
     *
     * @param string $vendorPath
     *
     * @throws UnexpectedValueException
     */
    public function __construct($vendorPath)
    {
        if (!is_string($vendorPath)) {
            throw new UnexpectedValueException('Expected ' . __CLASS__ . '::$vendorPath to be a string, ' . gettype($vendorPath) . ' was given.');
        }

        $this->vendorPath = $vendorPath;
        spl_autoload_register(array($this, 'load'));
    }

    /**
     * Load given class, interface or trait
     *
     * This method provides backwards compatibility and will be able to load classes
     * using underscores and namespace
     *
     * @param string $className
     */
    public function load($className)
    {
        $classPath = str_replace(array('\\', '_'), '/', ltrim($className, '/'));
        $fullPath = "{$this->vendorPath}/{$classPath}.php";
        
        if ($realPath = realpath($fullPath)) {
            require_once($realPath);
        }
    }
}