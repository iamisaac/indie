<?php

/**
 * @package Indie\Mvc
 */
namespace Indie\Mvc;

/**
 * @uses Indie\Mvc\View
 */
use Indie\Mvc\View;

/**
 * Controller
 */
class Controller
{
    /**
     * @var Indie\Mvc\View|null
     * @access protected
     */
    protected $view = null;

    /**
     * @var array
     * @access protected
     */
    protected $config = array();

    /**
     * Controller constructor
     *
     * @param array $config MVC configurations
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->view = new View($config['view_path']);
    }

    /**
     * Get model
     *
     * @param string $model Model name
     *
     * @return object
     */
    public function getModel($model)
    {
        $fullPath = "{$this->config['model_path']}/{$model}.php";
        require_once($fullPath);
        return new $model;
    }
}