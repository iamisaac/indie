<?php

/**
 * @package Indie\Mvc
 */
namespace Indie\Mvc;

/**
 * @uses InvalidArgumentException
 * @uses BadFunctionCallException
 */
use InvalidArgumentException;
use BadFunctionCallException;
use PDO;

/**
 * Model
 */
class Model
{
    /**
     * @var PDO|null Used if no database handler was provided
     */
    protected $pdo = null;

    /**
     * @var object|null Custom database handler
     */
    protected $dbh = null;

    /**
     * @var array database configuration
     */
    protected $config = array();

    /**
     * Model constructor
     *
     * @param array $config
     * @param object|null $dbh Database handler
     */
    public function __construct(array $config, $dbh = null)
    {
        $this->config = $config;

        if (!is_null($dbh)) {
            if (!is_object($dbh)) {
                throw new InvalidArgumentException('Database handler must be an object');
            }

            $this->dbh = $dbh;
        } else {
            if (!isset($config['dsn'], $config['username'], $config['password'])) {
                throw new BadFunctionCallException('Misconfiguration. Please check the dsn, username and/or password for errors');
            }
            $this->pdo = new PDO($config['dsn'], $config['username'], $config['password']);
        }
    }
}