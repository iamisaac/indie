<?php

/**
 * @package Indie\Mvc
 */
namespace Indie\Mvc;

/**
 * Router
 */
class Router
{
    /**
     * @var string Controller name
     */
    private $controller = '';

    /**
     * @var string Action name
     */
    private $action = '';

    /**
     * @var array Arguments that will be passed to the action
     */
    private $args = array();

    /**
     * Router constructor
     *
     * @param array $config
     * @param string $route
     */
    public function __construct(array $config, $route)
    {
        $this->config = $config;
        $route = trim($route, '/');

        if (empty($route)) {
            $this->controller = $this->config['default_controller'];
            $this->action = $this->config['default_action'];
        } else {
            if ($this->isAlias($route)) {
                $route = $config['router']['alias'][$route];
            }
            $this->parts = explode('/', trim($route, '/'));
            $this->controller = ucfirst($this->parts[0]) . 'Controller';
            $this->action = (isset($this->parts[1])) ? $this->parts[1] : $this->config['default_action'];
            $this->args = array_slice($this->parts, 2);
        }
    }

    /**
     * Check if the route is an alias
     *
     * @param string $route
     * 
     * @return boolean
     */
    private function isAlias($route)
    {
        return (array_key_exists($route, $this->config['router']['alias']));
    }

    /**
     * Route to the correct controller
     */
    public function redirect()
    {
        $basePath = $this->config['controller_path'];
        $fullPath = "{$basePath}/{$this->controller}.php";

        if ($realPath = realpath($fullPath)) {
            require_once($realPath);
            $object = new $this->controller($this->config);
            $object->{$this->action}($this->args);
        }
    }
}