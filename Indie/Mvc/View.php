<?php

/**
 * @package Indie\Mvc
 */
namespace Indie\Mvc;

/**
 * View
 */
class View
{
    /**
     * @var string Directory where the view file is located
     */
    private $viewPath = '';

    /**
     * @var string Extension of the view file
     */
    private $viewFileExtension = 'phtml';

    /**
     * View constructor
     *
     * @param string $viewPath
     * @param string $viewFileExtension
     */
    public function __construct($viewPath = '', $viewFileExtension = 'phtml')
    {
        $this->setViewPath($viewPath);
        $this->setViewFileExtension($viewFileExtension);
    }

    /**
     * Get header
     */
    public function getHeader()
    {
        $fullPath = "{$this->viewPath}/header.{$this->viewFileExtension}";
        require_once($fullPath);
    }

    /**
     * Get footer
     */
    public function getFooter()
    {
        $fullPath = "{$this->viewPath}/footer.{$this->viewFileExtension}";
        require_once($fullPath);
    }


    /**
     * Set the path where the view file is located
     *
     * @param string $viewPath
     */
    public function setViewPath($viewPath)
    {
        $this->viewPath = rtrim($viewPath, '/');
        if (!empty($this->viewPath)) {
            $parts = explode('/', $this->viewPath);
            $this->template = array_pop($parts);
        }
    }

    /**
     * Set view file extension
     *
     * @param string $viewFileExtension
     */
    public function setViewFileExtension($viewFileExtension)
    {
        $this->viewFileExtension = ltrim($viewFileExtension, '.');
    }

    /**
     * Render the output to the visitor
     *
     * @param string $view
     * @param array $args
     */
    public function render($view, array $args = array())
    {
        $fullPath = "{$this->viewPath}/{$view}.{$this->viewFileExtension}";

        if ($realPath = realpath($fullPath)) {
            extract($args);
            ob_start();
            require_once($realPath);
            $output = ob_get_contents();
            ob_end_clean();
            echo $output;
        }
    }
}