<?php

/**
 * @package Indie\Security\Cryptography
 */
namespace Indie\Security\Cryptography;

/**
 * @uses Indie\Security\Cryptography\Random
 */
use Indie\Security\Cryptography\Random;

/**
 * Hash
 */
class Hash
{
    const STD_DES = 1;
    const EXT_DES = 2;
    const MD5 = 3;
    const BLOWFISH_OLD = 4;
    const BLOWFISH = 7;
    const SHA256 = 5;
    const SHA512 = 6;

    /**
     * @var integer Hash method
     */
    public $algorithm = self::BLOWFISH;

    /**
     * Create hash
     *
     * @param integer $algorithm
     * @param array $options
     *
     * @return string
     */
    public function create($string, $algorithm = null, array $options = array())
    {
        $algorithm = (is_null($algorithm)) ? $this->algorithm : $algorithm;

        $random = new Random;

        switch ($algorithm) {
            case self::STD_DES:
                $length = 2;
                $format = '%s';
                break;

            case self::EXT_DES:
                break;

            case self::MD5:
                $length = 12;
                $format = '$1$%s$';
                break;

            case self::SHA256:
                $length = 16;
                $rounds = (!isset($options['rounds'])) ? 5000 : $options['rounds'];
                $rounds = (($rounds < 1000) || ($rounds > 999999999)) ? 5000 : $rounds;
                $format = '$5$rounds=' . $rounds . '$%s$';
                break;

            case self::SHA512:
                $length = 16;
                $rounds = (!isset($options['rounds'])) ? 5000 : $options['rounds'];
                $rounds = (($rounds < 1000) || ($rounds > 999999999)) ? 5000 : $rounds;
                $format = '$6$rounds=' . $rounds . '$%s$';
                break;

            case self::BLOWFISH_OLD:
            case self::BLOWFISH:
            default:
                $length = 22;
                $cost = (!isset($options['cost'])) ? '10' : $options['cost'];
                $cost = (($cost < '04') || ($cost > '31')) ? '10' : $cost;
                #$cost = str_pad($cost, 2, '0', STR_PAD_LEFT);
                if ($algorithm == self::BLOWFISH) {
                    $format = '$2y$' . $cost . '$%s$';
                } else {
                    $format = '$2a$' . $cost . '$%s$';
                }
                break;
        }

        if (!isset($options['salt'])) {
            $bytes = $random->bytes($length);
            $base64 = strtr(base64_encode($bytes), '+', '.');
            $data = substr($base64, 0, $length);
        } else {
            if (strlen($options['salt']) < $length) {
                return false;
            }

            $data = substr($options['salt'], 0, $length);
        }
        
        $salt = sprintf($format, $data);
        return crypt($string, $salt);
    }

    /**
     * Validate a plain text string with the given hash
     *
     * @param string $string
     * @param string $hash
     *
     * @return boolean
     */
    public function validate($string, $hash)
    {
        if (false !== (strstr($hash, '$'))) {
            return (crypt($string, $hash) === $hash);
        } else {
            switch (strlen($hash)) {
                case 32:
                    $algorithm = 'md5';
                    break;

                case 40:
                    $algorithm = 'sha1';
                    break;

                case 64:
                    $algorithm = 'sha256';
                    break;

                case 128:
                    $algorithm = 'sha512';
                    break;

                default:
                    return false;
            }

            return (hash($algorithm, $string) === $hash);
        }
    }
}