<?php

/**
 * @package Indie\Security\Cryptography
 */
namespace Indie\Security\Cryptography;

/**
 * Random
 */
class Random
{
    /**
     * Create random bytes
     *
     * Inspired by phpSec and Randomness
     *
     * @param integer $length
     * @param boolean $strong
     *
     * @return binary|boolean Byte string on success, false on failure
     */
    public function bytes($length, $strong = true)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $secure = false;
            $bytes = openssl_random_pseudo_bytes($length, $secure);
            if ($secure === true) {
                return $bytes;
            }
        }

        if (function_exists('mcrypt_create_iv')) {
            if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
                $source = MCRYPT_DEV_URANDOM;
            } else {
                $source = (PHP_OS == 'WINNT') ? MCRYPT_RANDOM : MCRYPT_DEV_URANDOM;
                srand($this->seed());
            }

            $bytes = mcrypt_create_iv($length, $source);
            return (binary) $bytes;
        }

        if (!$strong) {
            $url = sprintf('https://www.random.org/cgi-bin/randbyte?format=f&nbytes=%s', $length);
            if (false !== ($bytes = file_get_contents($url))) {
                return (binary) $bytes;
            }

            $sha =''; $rnd ='';
            for ($i=0; $i<$length; $i++) {
                $sha = hash('sha256',$sha.mt_rand());
                $char = mt_rand(0,62);
                $rnd .= chr(hexdec($sha[$char].$sha[$char+1]));
            }
            return (binary) $bytes;
        }

        return false;
    }

    /**
     * Create a pseudo random seed number
     * 
     * Based on example in PHP manual - http://www.php.net/manual/en/function.srand.php
     */
    public function seed()
    {
        list($usec, $sec) = explode(' ', microtime());
        return (float) $sec + ((float) $usec * 100000);
    }

    /**
     * Create a random hexadecimal string
     * 
     * @param integer $length
     *
     * @return string
     */
    public function hex($length)
    {
        return bin2hex($this->bytes($length));
    }

    /**
     * Create a random string
     *
     * @param integer $length
     *
     * @return string
     */
    public function string($length)
    {
        $base64 = base64_encode($this->bytes($length));
        return substr($base64, 0, $length);
    }
}