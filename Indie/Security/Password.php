<?php

/**
 * @package Indie\Security
 */
namespace Indie\Security;

/**
 * @uses Indie\Security\Cryptography\Hash
 */
use Indie\Security\Cryptography\Hash;

/**
 * Password
 */
class Password
{
    const STD_DES = 1;
    const EXT_DES = 2;
    const MD5 = 3;
    const BLOWFISH_OLD = 4;
    const BLOWFISH = 7;
    const SHA256 = 5;
    const SHA512 = 6;

    /**
     * @var integer Hashing algorithm
     */
    public $algorithm;

    /**
     * @var array Options to used when hashing such as cost for BLOWFISH and rounds for SHA
     */
    public $options;

    /**
     * @var object|null Instance of Indie\Security\Cryptography\Hash
     */
    private $hash = null;

    /**
     * Password constructor
     *
     * Example for $options array:
     * BLOWFISH    => ['cost' => '10']
     * SHA256      => ['rounds' => '5000']
     *
     * @param integer $algorithm Algorithm used when hashing
     * @param array $options Associative array of options related to the hashing algorithm
     */
    public function __construct($algorithm = self::BLOWFISH, array $options = ['cost' => '10'])
    {
        $this->hash = new Hash;
        $this->algorithm = $algorithm;
        $this->options = $options;
    }

    /**
     * Set algorithm
     *
     * @param integer $algorithm
     */
    public function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * Create password hash
     *
     * @param string $password
     * @param integer|null $algorithm
     * @param array $options
     *
     * @return string
     */
    public function hash($password, $algorithm = null, array $options = [])
    {
        $algorithm = (is_null($algorithm)) ? $this->algorithm : $algorithm;
        $options = (empty($options)) ? $this->options : $options;

        return $this->hash->create($password, $algorithm, $options);
    }

    /**
     * Validate plaintext password against hash
     *
     * @param string $password
     * @param string $hash
     *
     * @return boolean
     */
    public function validate($password, $hash)
    {
        return $this->hash->validate($password, $hash);
    }
}