Indie - An educactional project for self-education
======

```
in·die  
/ˈindē/
```

# Description

Indie is a personal project with an educational purpose. The goal for Indie is to build a
component library based on standalone packages that has no dependencies outside the 
package itself. The idea is to remove the rules and restrictions that all frameworks and 
most libraries today are forcing the developer to follow. The developer can grab only the 
packages that is needed for the current project and ignore everything else.

This project is ment to be for educational purposes only, and does not have any intentions 
of being a competitor to any other existing framework or library. It's all about 
self-education.

# Branches & Tags

## Branching

The project uses branching and taging actively. It has two main branches, which is:

  * _master_
  * _development_

No changes is done directly on the master branch, but in the appropriate branch type 
depending on what type of changes we're doing.

Besides these two, it has feature, pre-release, release and hotfix branches. 

### Feature branch

Each new feature that does not already exist
in the project will first be a feature branch. Some feature branches might be empty, this
is because a new feature branch will be created for all features planned for future. 
When the component is ready to be included into the project it is merged into the 
development branch.

**Name format:** package-component[-subcomponent]

**Examples:**

WebServices\Google\YouTube -> ws-google-youtube (Using sub-component)  
PaymentGateways\PayPal -> paygate-paypal

### Pre-release branches

When the development branch reaches a state where it is getting ready to be released it 
is checked out to a pre-release branch. No new features will be added in this stage 
unless it is absolutely necessary. Any bugs that is found in this stage is fixed directly 
on this branch.

**Name format:** prel-version

**Examples:** prel-0.1.0

### Release branches

This is the final stage before a new official release. It's a small stage but essential. 
After checking out the pre-release branch to the release branch all files will be 
modified to display the new version number. When this is done all changes is commited 
and the release branch is merged into master.

**Name format:** release-version

**Example:** release-0.1.0

### Hotfix branches

Hotfix branches are used only when critical bugs has been found in a stable release. The 
master branch is then checked out into a hotfix branch and when the bug has been fixed 
the changes is merged back in to master and development. 

**Name format:** hotfix-version

**Example:** hotfix-0.1.0

## Tagging

Tags are mainly used as a version archive, but might be used for other things as well.

**Name format:** release-version

**Example:** 0.1.0

Requirements
------
Indie follows PSR-0, PSR-1 and PSR-2, but will not include PSR-3 since this imposes 
unnecessary dependencies.

All dependencies will be included using the `use` operator right below the namespace.

All classes, methods, constants and properties is also well documented. This includes 
DocBlock tags for all parameters, returns, use operators, exceptions, and so on.